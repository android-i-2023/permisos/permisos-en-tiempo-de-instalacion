package com.utn.ejemplo.permisos

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import com.google.android.material.snackbar.Snackbar

class PermisosPeligrososActivity : AppCompatActivity(), View.OnClickListener {
    private val COD_PERMISO_LLAMADA = 29384757

    private val permisoParaLlamar = Manifest.permission.CALL_PHONE
    private lateinit var nroTelefono: EditText
    private lateinit var raiz: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permisos_peligrosos)

        raiz = findViewById(R.id.raiz)
        nroTelefono = findViewById(R.id.edt_numero_telefono)
        val iniciar = findViewById<Button>(R.id.btn_iniciar)
        iniciar.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (tenemosPermisoParaLlamar()) {
            llamar()
        } else if (deberiamosExplicarParaQueNecesitamosPermisoParaLlamar()) {
            explicarPorQueNecesitamosPermisoParaLlamar()
        } else {
            solicitarPermisoParaLlamar()
        }
    }

    private fun tenemosPermisoParaLlamar(): Boolean {
        return PermissionChecker.checkSelfPermission(
            this,
            permisoParaLlamar
        ) == PermissionChecker.PERMISSION_GRANTED
    }

    private fun tenemosPermisoParaLlamar(resultado: Int): Boolean {
        return resultado == PermissionChecker.PERMISSION_GRANTED
    }

    private fun deberiamosExplicarParaQueNecesitamosPermisoParaLlamar(): Boolean {
        return ActivityCompat.shouldShowRequestPermissionRationale(this, permisoParaLlamar)
    }

    private fun explicarPorQueNecesitamosPermisoParaLlamar() {
        val snackbar = Snackbar.make(
            this,
            raiz,
            getString(R.string.motivo_solicitud_permiso_llamada),
            Snackbar.LENGTH_INDEFINITE
        )

        snackbar.setAction(getString(R.string.ok)) {
            solicitarPermisoParaLlamar()
        }

        snackbar.show()
    }

    private fun solicitarPermisoParaLlamar() {
        ActivityCompat.requestPermissions(this, arrayOf(permisoParaLlamar), COD_PERMISO_LLAMADA)
    }

    private fun llamar() = marcarNumero(nroTelefono.text.toString())

    private fun marcarNumero(nroTelefono: String) {
        val intent = Intent(Intent.ACTION_CALL)

        // Uri -> Universal Resource Identifier
        // "tel" es el _esquema_ de la Uri
        intent.data = Uri.parse("tel:$nroTelefono")

        startActivity(intent)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        /*
            permissions y grantResults son arrays paralelos
            permissions[0] tiene el primer permiso que pedimos, y grantResults[0] indica si el usuario acepto o no conceder ese permiso

            En esta funcion no usamos permissions[0] porque estamos pidiendo solamente un permiso y es obvio cual es
         */

        when (requestCode) {
            COD_PERMISO_LLAMADA -> {
                if (tenemosPermisoParaLlamar(grantResults[0])) {
                    llamar()
                } else {
                    Toast.makeText(this, getString(R.string.permiso_denegado), Toast.LENGTH_LONG)
                        .show()
                }
            }
        }
    }
}