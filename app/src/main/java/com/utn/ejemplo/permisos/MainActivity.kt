package com.utn.ejemplo.permisos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnPermisosNormales = findViewById<Button>(R.id.permisos_normales)
        val btnPermisosPeligrosos = findViewById<Button>(R.id.permisos_peligrosos)

        btnPermisosNormales.setOnClickListener(this)
        btnPermisosPeligrosos.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        val claseActivity = if (v.id == R.id.permisos_normales) {
            PermisosNormalesActivity::class.java
        } else {
            PermisosPeligrososActivity::class.java
        }
        startActivity(Intent(this, claseActivity))
    }
}