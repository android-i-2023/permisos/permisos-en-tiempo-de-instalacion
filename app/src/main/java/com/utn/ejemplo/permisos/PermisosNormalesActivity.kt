package com.utn.ejemplo.permisos

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.net.Inet4Address

class PermisosNormalesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permisos_normales)

        val txtIps = findViewById<TextView>(R.id.txt_ips)

        val btnObtenerIps = findViewById<View>(R.id.btn_obtener_ips)
        btnObtenerIps.setOnClickListener {
            val ips = obtenerIps()
            txtIps.text = ips.joinToString("\n")
        }
    }

    private fun obtenerIps(): List<String> {
        val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val redActiva = manager.activeNetwork

        if (redActiva == null) {
            Toast.makeText(this, "No tenemos red", Toast.LENGTH_SHORT).show()
            return emptyList()
        }

        val direcciones = manager.getLinkProperties(redActiva)?.linkAddresses
        val direccionesIPv4 = direcciones?.filter {
            it.address is Inet4Address
        }
        val ips = direccionesIPv4?.map { it.address.hostAddress }

        if (ips == null || ips.isEmpty()) {
            Toast.makeText(this, "No estamos conectados a la red", Toast.LENGTH_SHORT).show()
            return emptyList()
        }

        return ips
    }
}
